function [ ] = listar( )
    FileName = 'jogos.mat';
    jogos = carregar_jogos(FileName);
    [rows,columns] = size(jogos);
    for i = 1: columns
        
        fprintf('\n%d: %s\n',i,datestr(jogos(i).data,'HH:MM dd-mm-yyyy'));
        fprintf('Navios: %d\n', contar_emb(jogos(i).posicoes_x));
        
        [lancados_r, lancados_c] = size(jogos(i).p_x);
        fprintf('M�sseis lancados: %d\n', lancados_r);
        
        [atingidos_r, atingidos_c] = size(find(jogos(i).dentro == 1));
        fprintf('Navios atingidos: %d\n',atingidos_r);
    end

    clear jogos;
end

