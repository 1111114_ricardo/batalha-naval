function [ jogos ] = carregar_jogos( fileName )
    jogos = [];
    if exist(fileName, 'file')
        load(fileName, 'jogos');
    end
    
end

