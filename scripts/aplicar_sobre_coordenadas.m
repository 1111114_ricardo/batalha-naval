function [emb_x,emb_y] = aplicar_sobre_coordenadas(emb_x,emb_y, rotacao, init_p_x, init_p_y)

    [rows, columns] = size(emb_x);
    for i = 1 :  columns
        x = emb_x(1,i);
        y = emb_y(1,i);
        
        % x' = x cos f - y sin f
        % y' = y cos f + x sin f
        % https://www.siggraph.org/education/materials/HyperGraph/modeling/mod_tran/2drota.htm
        emb_x(1,i) = (x * cosd(rotacao)) - (y * sind(rotacao));
        emb_y(1,i) = (y * cosd(rotacao)) + (x * sind(rotacao));
    end
    
    emb_x = emb_x + init_p_x;
    emb_y = emb_y + init_p_y;
    
end

