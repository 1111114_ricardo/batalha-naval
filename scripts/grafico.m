function [] = grafico(posicoes_x,posicoes_y,p_x,p_y,cores,dentro)
    figure
    title('Introdu��o � Programa��o');
    xlabel('X(milhas)');
    ylabel('Y(milhas)');
    hold on
    
    num_emb = contar_emb(posicoes_x);
    
    for i = 1: num_emb
        poligno_x = posicoes_x(i,:);
        poligno_y = posicoes_y(i,:);
        
        % Representar poligono graficamente:
        fill(poligno_x, poligno_y, cores{i,2})
        
    end

    [rows, columns] = size(p_x);
    for i = 1:rows
        x = p_x(i,1);
        y = p_y(i,1);
        plot(x,y, 'r*-');
        
    end
    
    legend(cores(:,1));
    
% Definir os limites dos eixos da figura:
    set(gca, 'xlim', [0,0.5])
    set(gca, 'ylim', [0,0.5])
    
end