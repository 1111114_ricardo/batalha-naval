function esp = aleatorio()
    esp = -1;
    ck_in = 0;
    while(not(ck_in) || (esp ~= 1 && esp ~= 0))
        esp = input('\nDeseja especificar as embarcações e as suas posições?\n1 - Especificar embarcações\n0 - Gerar aleatóriamente\n','s');
        [ck_in, esp] = verificar_input(esp,'double');
    end
end