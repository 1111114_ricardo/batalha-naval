function guardar(posicoes_x,posicoes_y,p_x,p_y,cores,dentro )

    guardar = -1;
    ck_in = 0;
    while(not(ck_in) || (guardar ~= 1 && guardar ~= 0))
        guardar = input('\nDeseja guardar o jogo?\n1 - Sim\n0 - Nao\n','s');
        [ck_in, guardar] = verificar_input(guardar,'double');
    end
    
    if guardar
        FileName = 'jogos.mat';

        jogos = carregar_jogos(FileName);
        [rows,columns] = size(jogos);
        num = columns + 1;
        jogos(num).posicoes_x = posicoes_x;
        jogos(num).posicoes_y = posicoes_y;
        jogos(num).p_x = p_x;
        jogos(num).p_y = p_y;
        jogos(num).cores = cores;
        jogos(num).dentro = dentro;
        jogos(num).data = datetime('now');

        save(FileName,'jogos');
    end
    
end

