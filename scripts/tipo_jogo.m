function novo = tipo_jogo()
    novo = -1;
    ck_in = 0;
    while(not(ck_in) || (novo ~= 1 && novo ~= 0))
        novo = input('\nDeseja iniciar um novo jogo?\n1 - Sim\n0 - Nao\n','s');
        [ck_in, novo] = verificar_input(novo,'double');
    end
end

