nome: [emb_x,emb_y] = aplicar_sobre_coordenadas(emb_x,emb_y, rotacao, init_p_x, init_p_y)
INICIO
	calcula columns (size(emb_x))

	ENQUANTO i < columns ENTAO
		atribui valor a x (emb_x(1,i))
		atribui valor a y (emb_y(1,i))

		calcula novo valor a emb_x(1,i) = (x * cosd(rotacao)) - (y * sind(rotacao))
		calcula novo valor a emb_y(1,i) = (y * cosd(rotacao)) + (x * sind(rotacao))
	FIM

	calcula novas coordenadas de emb_x (emb_x + init_p_x)
	calcula novas coordenadas de emb_y (emb_y + init_p_y)

FIM