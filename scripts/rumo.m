function [emb_x,emb_y] = rumo(emb_x,emb_y)
    min= 0;
    max = 0.5;

    pos_x = (max - min)* rand(1,1) + min;
    pos_y = (max - min)* rand(1,1) + min;
%     %fprintf('%d %d \n', pos_x, pos_y);
    
%     rumo = 90;
    rumo = 360 * rand(1,1);

    [emb_x, emb_y] = aplicar_sobre_coordenadas(emb_x, emb_y, rumo, pos_x, pos_y);
    
end
