function [p_x,p_y,dentro] = atingir_emb(posicoes_x,posicoes_y,num)
    p_x = [];
    p_y = [];
    dentro = [];
%     display(posicoes_x);
%     display(posicoes_y);
    while perguntar_at(num)
        [p_x_t,p_y_t,dentro_t] = atingir(posicoes_x,posicoes_y);
        
        p_x = [p_x;p_x_t];
        p_y = [p_y;p_y_t];
        dentro = [dentro; dentro_t];
        
%         display(p_x);
%         display(p_y);
%         display(dentro);
    end
    
    %concatenar([8000;5000],[9000;4000],[0;1],p_x,p_y,dentro);
    
end