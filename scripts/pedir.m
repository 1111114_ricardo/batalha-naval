function num = pedir()
    num = -1;
    ck_in = 0;
    while(not(ck_in) || (num < 1 || num > 10))	
        num = input('\nInsira o nr de embarcações que deseja colocar: ','s');
        [ck_in, num] = verificar_input(num,'double');
    end
end