function [embs_x,embs_y,cores] = lernavios()
    embarcacao_str = 'Embarcação:';
    cor_str = 'Cor:';
    [emb,txt] = xlsread('Embarcações.xlsx');
    
    [rows, columns] = size(emb);
    embs_x = zeros(rows/2, columns);
    embs_y = zeros(rows/2, columns);
    cores = cell(rows/2, 2);
    
    %leitura das coordenadas
    k = 1;
    for i = 1:rows
        if mod(i, 2) == 0
          embs_y(k,:) = emb(i,:);
          k = k + 1;
        else
          embs_x(k,:) = emb(i,:);
        end
    end
    
    %leitura das classes e cores
    [rows, columns] = size(txt);
    k = 1;
    for i = 1: columns
        
        if strcmp(txt(1,i), embarcacao_str)
            for j  = 2 : rows
                if(strcmp(txt(j, i), '') == 0)
                    cores(k,1) = txt(j,i);
                    k = k+1;
                end
            end
            k = 1;
        elseif strcmp(txt(1,i), cor_str)
            for j  = 2 : rows
                if(strcmp(txt(j, i), '') == 0)
                    cores(k,2) = txt(j,i);
                    k = k+1;
                end
            end
            k = 1;
        end
    end
    
%     display(embs_x);
%     display(embs_y);
%     display(cor);
end

