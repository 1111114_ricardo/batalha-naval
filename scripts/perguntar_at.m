function at = perguntar_at(num)
    at = -1;
    ck_in = 0;
    while(not(ck_in) || (at ~= 1 && at ~= 0))
        fprintf('\nEst�o %d embarca��es na �rea. Deseja lancar m�sseis para tentar atingir embarca��es?\n1-Sim\n0-Nao\n', num);
        at = input('','s');
        [ck_in, at] = verificar_input(at,'double');
    end
end