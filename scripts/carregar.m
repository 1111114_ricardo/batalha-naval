function [ posicoes_x,posicoes_y,p_x,p_y,cores,dentro ] = carregar( )

    FileName = 'jogos.mat';
    jogos = carregar_jogos(FileName);
    [rows,columns] = size(jogos);
    
    num = -1;
    ck_in = 0;
    while(not(ck_in) || (num < 1 || num > columns))	
        num = input('\nInsira o nr de jogo que pretende carregar: ','s');
        [ck_in, num] = verificar_input(num,'double');
    end

    posicoes_x = jogos(num).posicoes_x;
    posicoes_y = jogos(num).posicoes_y;
    p_x = jogos(num).p_x;
    p_y = jogos(num).p_y;
    cores = jogos(num).cores ;
    dentro = jogos(num).dentro ;
    
    clear jogos;
    
end

