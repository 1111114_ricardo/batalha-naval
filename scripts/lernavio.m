function [emb_x,emb_y,cor] = lernavio()
    [embs_x,embs_y,cores] = lernavios;
    a = 1;
    b = contar_emb(embs_x); %/**limite de barcos **/

    op = round((b - a)* rand(1,1) + a);
    emb_x = embs_x(op,:);
    emb_y = embs_y(op,:);
    cor = cores(op,:);
end
