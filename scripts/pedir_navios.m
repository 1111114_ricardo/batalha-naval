function [posicoes_x,posicoes_y,cores] = pedir_navios(num)
    posicoes_x = zeros(num, 6);
    posicoes_y = zeros(num, 6);
    cores = cell(num,2);
    x = 1;
    while(x <= num)
        [posicao_x, posicao_y,cor] = mostrar_navios();
        [posicao_x_con, posicao_y_con] = converter(posicao_x, posicao_y);
        [posicao_x_con,posicao_y_con] = pedir_rumo(posicao_x_con, posicao_y_con);
        
        % roundn - Descartada precisao a partir das 4 casas decimais
        posicoes_x(x,:) = roundn(posicao_x_con, -4);
        posicoes_y(x,:) = roundn(posicao_y_con, -4);
        cores(x,:) = cor;
        
        x = x + 1;
    end


end

