function [emb_x,emb_y,cor] = mostrar_navios()

    [embs_x,embs_y,cores] = lernavios;
    b = contar_emb(embs_x); %/**limite de barcos **/

    for i = 1: b
        nome = cores{i,1};
        fprintf('%d. %s\n',i, nome);
    end
    
    op = -1;
    ck_in = 0;
   while(not(ck_in) || (op < 1 || op > b))
       op = input('\nEscolha uma das embarcaçoes referidas acima: ','s');
       [ck_in, op] = verificar_input(op,'double');
   end
    
    emb_x = embs_x(op,:);
    emb_y = embs_y(op,:);
    cor = cores(op,:);
end

