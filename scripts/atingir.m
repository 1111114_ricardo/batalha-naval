function [p_x,p_y,dentro] = atingir(posicoes_x,posicoes_y)
    dentro = 0;
    
    ck_in = 0;
    p_x = -1;
    while(not(ck_in) || (p_x < 0 || p_x > 0.5))
       p_x = input('\nIntroduza a coordenada X (valores entre 0 - 0.5): ','s');
       [ck_in, p_x] = verificar_input(p_x, 'double');
    end
    
    p_y = -1;
    ck_in = 0;
    while(not(ck_in) || (p_y < 0 || p_y > 0.5))
       p_y = input('\nIntroduza a coordenada Y (valores entre 0 - 0.5): ','s');
       [ck_in, p_y] = verificar_input(p_y, 'double');
    end
    
    
%     [p_x,p_y] = converter(p_x,p_y);
    for i = 1:contar_emb(posicoes_x)
        if inpolygon(p_x, p_y, posicoes_x(i,:), posicoes_y(i,:))
            dentro = 1;    
            fprintf('Atingiu uma embarcação! (%f, %f)\n', p_x, p_y);
        end
    end

end