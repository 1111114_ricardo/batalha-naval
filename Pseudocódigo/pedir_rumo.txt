nome: pedir_rumo(emb_x, emb_y)
INICIO

	ck_in = 0
	pos_x = -1
	ENQUANTO ck_in = 0 OU pos_x < 0 OU pos_x > 0.5 ENTAO
		escrever (“Introduza a posição x inicial da embarcação”)
		LE(pos_x)
		calcula ck_in verificando o input (pos_x)
	FIM

	ck_in = 0
	pos_y = -1
	ENQUANTO ck_in = 0 OU pos_y < 0 OU pos_y > 0.5 ENTAO
		escrever (“Introduza a posição y inicial da embarcação”)
		LE(pos_y)
		calcula ck_in verificando o input (pos_y)
	FIM

	ck_in = 0
	rumo = -1
	ENQUANTO ck_in = 0 OU rumo < 0 OU rumo > 360 ENTAO
		escrever (“Introduza o valor do rumo”)
		LE(rumo)
		calcula ck_in verificando o input (rumo)
	FIM

	calcula novas coordenadas para emb_x e emb_y através da função aplicar_sobre_coordenadas

RETORNA emb_x, emb_y
FIM