function [emb_x,emb_y] = pedir_rumo(emb_x,emb_y)

   pos_x = -1;
   ck_in = 0;
   while(not(ck_in) || (pos_x < 0 || pos_x > 0.5))
       pos_x = input('\nIntroduza a posi��o x inicial da embarca��o (valores entre 0 - 0.5):','s');
       [ck_in, pos_x] = verificar_input(pos_x, 'double');
   end

   pos_y = -1;
   ck_in = 0;
   while(not(ck_in) || (pos_y < 0 || pos_y > 0.5))
       pos_y = input('\nIntroduza a posi��o y inicial da embarca��o (valores entre 0 - 0.5):','s');
       [ck_in, pos_y] = verificar_input(pos_y, 'double');
   end
   
   rumo = -1;
   ck_in = 0;
   while(not(ck_in) || (rumo < 0 || rumo > 360))
       rumo = input('\nIntroduza o valor do rumo (0 - 360):','s');
       [ck_in, rumo] = verificar_input(rumo, 'double');
   end

   [emb_x, emb_y] = aplicar_sobre_coordenadas(emb_x, emb_y, rumo, pos_x, pos_y);
   
end