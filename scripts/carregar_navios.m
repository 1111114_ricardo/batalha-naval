function [CORES, VASCO_GAMA,BARTOLOMEU_DIAS,NRP_BERRIO,VIANA_CASTELO,TRIDENTE ] = carregar_navios()
    CORES = ['y','b','k','c','m'];
    VASCO_GAMA = [0	80 115.9 80 0 0;0 0 7.1	14.2 14.2 0];	%/** [cordenadas_x ; coordenadas_y ] **/
    BARTOLOMEU_DIAS = [0 85	122 85	0 0;0 0	7.2 14.4 14.4 0];
    NRP_BERRIO = [0	127 140.6 127 0	0;0 0 9.6 19.2 19.2 0];
    VIANA_CASTELO = [0 60 83.1 60 0 0; 0 0 6.475 12.95 12.95 0];
    TRIDENTE = [0 2.9 65 67.9 65 2.9; 3.15 0 0 3.15 6.3 6.3];
end