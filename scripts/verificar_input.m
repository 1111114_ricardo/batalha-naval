function [ correcto , input] = verificar_input( input, tipo_desejado )
    correcto = 0;
    if(isempty(input))
        correcto = 0;
    elseif strcmp(tipo_desejado, 'double')
        input = str2double(input);
        correcto = isa(input, 'double');
        if(correcto && isnan(input))
            correcto = 0;
        end
    end
end



